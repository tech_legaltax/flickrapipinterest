// Main JavaScript

    var sampleApp = angular.module('lBBApp', []);
    
    sampleApp.controller('MapCtrl', function ($scope, $http) {

      var mapOptions = {
          zoom: 4,
          center: new google.maps.LatLng(25,80),
          mapTypeId: google.maps.MapTypeId.TERRAIN
      }
      $scope.markers = [];

      var infoWindow = new google.maps.InfoWindow();
      var map = new google.maps.Map(document.getElementById('mapper'), mapOptions);
      navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      };

      $scope.lat=position.coords.latitude;
      $scope.longi=position.coords.longitude;
      infoWindow.setPosition(pos);
      infoWindow.setContent('Location found.');
      infoWindow.open(map);
      map.setCenter(pos);
      }, function() {
        handleLocationError(true, infoWindow, map.getCenter());
      });

      var flag=0;
      google.maps.event.addListener(map, 'click', function (e) {
        if(flag==1){
          $scope.mymarker.setMap(null);
        }
        $scope.lat=e.latLng.lat();
        $scope.longi=e.latLng.lng();

        //add marker
        $scope.mymarker = new google.maps.Marker({
          map: map,
          position: new google.maps.LatLng($scope.lat, $scope.longi)
        });
        flag=1;
      });

        $scope.searchButtonText = "Search";
		$scope.logId = function() {
		console.log($scope.searchBox);
		$scope.loading = true;
		$scope.searchButtonText = "Searching";

		$http.jsonp('https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=c6cbd1445febf9830208782f6444f759&format=json&lat='+$scope.lat+'&lon='+$scope.longi+'&tags='+$scope.searchBox).success(function(data){

		});                                     
		$scope.foundit="true";

		jsonFlickrApi=function (data) {
		  $scope.viewby = 10;
		  $scope.totalItems = data.length;
		  $scope.maxSize = 5; //Number of pager buttons to show
		  $scope.itemsPerPage = 10;
		  $scope.currentPage = 1; //reset to first paghe
		  $scope.images=data.photos.photo;
		  console.log($scope.images);
		  $scope.loading = false;
		  $scope.searchButtonText = "Search";
		}
		}
  	});
	
    sampleApp.controller('SearchCtrl', function ($scope, $http) {
    
    });

    angular.module('lBBApp').filter('pagination', function()
    {
      return function(input, start)
      {
        start = +start;
        return input.slice(start);
      };
    });
